//Problem 1: Write a function that will return a particular board's information based on
// the boardID that is passed from the given list of boards in boards.json and then pass 
//control back to the code that called it by using a callback function.

const fs=require('fs');

const boards='./data/boards.json';

function boardInformation(id,callback){
   
      fs.readFile(boards,'utf-8',(error,data)=>{
           if(error){
            console.error('Error is:',error.message);
            setTimeout(callback,2000,error,null);
           }
           const boards = JSON.parse(data);
        var foundBoard = undefined;
        boards.forEach((board) => {
            if (board.id === id) {
                foundBoard = board;
            }
        });
    //  console.log(foundBoard);
        if(foundBoard!==undefined){
            setTimeout(callback,2000,null,foundBoard);
        }else{
            return null;
        }
        //return foundBoard;
   });
}

//boardInformation('mcu453ed');

module.exports=boardInformation;