//Problem 2: Write a function that will return all lists that belong to a board based on the
 //boardID that is passed to it from the given data in lists.json. 
 //Then pass control back to the code that called it by using a callback function.

const fs=require('fs');

const lists='./data/lists_1.json';

function lists_Information(id,callback){
   
      fs.readFile(lists,'utf-8',(error,data)=>{
           if(error){
            console.error('Error is:',error.message);
            setTimeout(callback,2000,error,null);
           }
           const lists = JSON.parse(data);
        var findList = undefined;

          if(lists[id]){
              findList = lists[id];
            }
    //  console.log(foundBoard);
        if(findList!==undefined){
            setTimeout(callback,2000,null,findList);
        }else{
          return null;
        }

        //return findList;
                 
   });
}


module.exports=lists_Information;