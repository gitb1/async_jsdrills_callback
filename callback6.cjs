/* 
	Problem 6: Write a function that will use the previously written functions to get the following 
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const fs = require("fs");
const boardInformation = require("./callback1.cjs");
const listsInformation = require("./callback2.cjs");
const cardInformation = require("./callback3.cjs");
const boardsPath = "./data/boards.json";

function getAllCardsFromThanosBoard() {
  fs.readFile(boardsPath, "utf-8", (error, data) => {
    if (error) {
      console.error("Error:", error.message);
      return;
    }

    const boards = JSON.parse(data);
    const thanosBoard = boards.find((board) => board.name === "Thanos");

    if (!thanosBoard) {
      console.log("Thanos board not found.");
      return;
    }

    const thanosBoardId = thanosBoard.id;

    boardInformation(thanosBoardId, (error, boardInfo) => {
      if (error) {
        console.error("Error:", error.message);
        return;
      }
      console.log("Board Information:", boardInfo);
    });

    listsInformation(thanosBoardId, (error, lists) => {
      if (error) {
        console.error("Error:", error.message);
        return;
      } else {
        console.log("Lists in Thanos board:", lists);

        let totalLists = lists.length;
        let cardsCount = 0;

        lists.forEach((list) => {
          cardInformation(list.id, (error, cards) => {
            if (error) {
              console.error("Error:", error.message);
              return;
            } else {
                console.log(`Cards in ${list.name} list:`, cards);
                cardsCount++;
                if (cardsCount === totalLists) {
                  console.log("All Cards retrieved.");
                }
            }
          });
        });
      }
    });
  });
}

//getAllCardsFromThanosBoard();

module.exports = getAllCardsFromThanosBoard;
