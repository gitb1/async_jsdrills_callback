const fs = require("fs");
const boardInformation = require("./callback1.cjs");
const listsInformation = require("./callback2.cjs");
const cardInformation = require("./callback3.cjs");
const boardsPath = "./data/boards.json";

function ThanosInfo(name) {
  fs.readFile(boardsPath, "utf-8", (error, data) => {
    if (error) {
      console.error("Error:", error.message);
      return; // Return to prevent further execution
    }

    const boards = JSON.parse(data);
    const foundBoard = boards.find((board) => board.name === name);

    if (!foundBoard) {
      console.log("Board not found.");
      return;
    }

    const boardId = foundBoard.id;

    boardInformation(boardId, (error, boardInfo) => {
      if (error) {
        console.error("Error:", error.message);
        return;
      }
      console.log("Board Information:", boardInfo);
    });
    var id = undefined;
    listsInformation(boardId, (error, lists) => {
      if (error) {
        console.error("Error:", error.message);
        return;
      }
      console.log("List in Thanos board", lists);

      const mindList = lists.find((list) => list.name === "Mind");
      if (!mindList) {
        console.log("Mind list not found.");
        return;
      }

      const mindListId = mindList.id;

      cardInformation(mindListId, (error, cards) => {
        if (error) {
          console.error("Error:", error.message);
          return;
        }
        console.log("Cards in Mind List:", cards);
      });
    });
  });
}

module.exports = ThanosInfo;
