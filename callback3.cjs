//Problem 3: Write a function that will return all cards that belong to a particular list based 
//on the listID that is passed to it from the given data in cards.json. Then pass control back 
//to the code that called it by using a callback function.


 const fs= require('fs');

 const cards='./data/cards.json';
 
 function cardInformation(id,callback){
    
       fs.readFile(cards,'utf-8',(error,data)=>{
            if(error){
             console.error('Error is:',error.message);
             setTimeout(callback,2000,error,null);
             
            }
            const cards = JSON.parse(data);
         var findCard = undefined;
 
           if(cards[id]){
               findCard = cards[id];
             }
     //  console.log(foundBoard);
         if(findCard!==undefined){
            
             setTimeout(callback,2000,null,findCard);
         }else{
              console.log("NO card available for  list id",id);
         }
       //return findCard;
    });
 }
 
 
 module.exports=cardInformation;